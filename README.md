########## Setting up the webhook and playbook

Register the incoming webhook

	1.	Open Hangouts Chat in your browser.
	
	2.	Go to the room to which you want to add a bot.
	
	3.	From the menu at the top of the page, select Configure webhooks.

	4.	Under Incoming Webhooks, click ADD WEBHOOK.
	
	5.	Name the new webhook 'Quickstart Webhook' and click SAVE.
	
	6.	Copy the URL listed next to your new webhook in the Webhook Url column to the group_vars/all.yml file's googlechat_room variable.
	
	7.	Click outside the dialog box to close.

The playbook launches the postchat.py script which posts a comment to the chat's webhook using a bot

########## Running this from Ansible Tower

Add the playbook files and folders to a Bitbucket repository and link this to an Ansible Tower project

Synchronise the project to Tower and the playbook will appear in the dropdown list of playbooks available in the project when you go to add the playbook to a job template

Create the job template for the chat alert and select the googlechatbox.yml playbook

Select the correct credentials to log in to the local tower host including selecting the correct private key

Add the following variables to the job template (edit the values as is required)

ansible_connection: local

googlechat_room: "https://chat.googleapis.com/v1/spaces/AAAArVAJ2_k/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=pSvnoqwMudjkRRtEldytwcUkW-yjSpOlPP65gTWHTAo%3D"

googlechat_status: "SUCCESS"

googlechat_msg: "THIS IS A TEST FROM TOWER"

googlechat_conversation: "GOOGLECHAT ALERT"

Save the job template

This job template can now be used in any workflow job template to alert the GoogleChat room that the job has been completed
