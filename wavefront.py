import time
import csv
import os
import sys
import platform
from time import sleep
from json import dumps

# Hangouts Chat incoming webhook quickstart
# https://developers.google.com/hangouts/chat/quickstart/incoming-bot-python

vOS = "/"

if "Windows" in str(platform.system()):
    vOS = "\\"

def DateNow():
    seconds = time.time()
    return str(seconds)

def main():
    print("launching python <url> <token> <text> <info>")

    vurl = sys.argv[1]
    vtoken = sys.argv[2]
    vtext = sys.argv[3]
    vinfo = sys.argv[4]
    vstart = DateNow()
    vfinish = DateNow()
    vwavecmd = "curl -X POST --header \"Content-Type: application/json\" --header \"Accept: application/json\" --header \"Authorization: Bearer " + vtoken + "\" -d \"{\\\"name\\\": \\\"Wavefront Event\\\",\\\"annotations\\\": {\\\"severity\\\": \\\"info\\\",\\\"type\\\": \\\"event type\\\",\\\"details\\\": \\\"" + vtext + "\\\"},\\\"tags\\\" : [\\\"eventTag1\\\"],\\\"startTime\\\": " + vstart + ",\\\"" + vfinish + "\\\": 1490000000001}\" \"" + vurl + "\""
    os.system(vwavecmd)
    print("completed")

if __name__ == '__main__':
    main()
